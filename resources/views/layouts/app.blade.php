<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        @yield('tab-title')

        <link rel="icon" href="{{ URL('/N-Logo.png') }}" type="image/png" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        <livewire:styles>
    </head>

    <body>

        <header class="header_area white-header">
            <div class="main_menu">

                <nav class="navbar navbar-expand-lg navbar-dark fixed-top" >
                    <div class="container">

                        <a class="navbar-brand" href="{{ URL('/') }}"> NATSIR
                            {{-- <img src="{{ url('/N-Logo.png') }}" alt="logo" style="width:40px;"/> --}}
                        </a>
                        <button
                            class="navbar-toggler"
                            type="button"
                            data-toggle="collapse"
                            data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent"
                            aria-expanded="false"
                            aria-label="Toggle navigation"
                        >
                            <span class="icon-bar"></span> <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                            <ul class="nav navbar-nav menu_nav ml-auto">
                                <li class="nav-item"><a class="nav-link" href="{{ URL('/about') }}">About</a></li>
                                <li class="nav-item"><a class="nav-link" href="{{ URL('/about') }}">Blog</a></li>
                                <li class="nav-item"><a class="nav-link" href="{{ URL('/contact') }}">Contact</a></li>
                                @yield('menu')

                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </header>

        @yield('content')


        {{--<script src="{{ mix('js/app.js') }}"></script>--}}
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
        <livewire:scripts>
        <script src="https://unpkg.com/react@16/umd/react.development.js" crossorigin></script>
        <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js" crossorigin></script>

        <!-- Load our React component. -->
        <script src="js/like_button.js"></script>
    </body>
</html>
