@extends('layouts.app')

@section('menu')

@endsection

@section('content')

    {{--<div class="flex-center position-ref full-height">

        <div class="content">

            <div class="title m-b-md">

                <h1>Welcome to my <span>Portofolio</span> </h1>
            </div>
            <div class="links">
                <livewire:couter></livewire:couter>
            </div>


        </div>
    </div> --}}

    <section class="intro">
        <div class="intro-left">
          <img src="welcome-01.png" alt="a laptop on a wood table">
        </div>
        <div class="intro-middle">
          <h2>Welcome to my Portofolio</h2>

          <p>Here in this web you can see my work from past project that I have done.</p>
        </div>
        <div class="intro-right">
          <img src="welcome-02.png" alt="two computers and a laptop on a table">
        </div>
    </section>

      <section class="about-me" id="about">
        <img class="about-img" src="natsir.jpg" alt="A picture of Joe Portfolio">
        <h1 class="about-title">Natsir Alghifari</h1>
        <h2 class="about-subtitle">Developer</h2>
        <div class="about-text">
          <p>Aenean mattis tristique elementum. Duis massa tellus, tempus non fermentum at, venenatis et augue. Phasellus tristique purus sed sagittis interdum. Duis luctus sapien justo, vel viverra ex convallis et. Maecenas suscipit lacus ut lectus mattis ornare. Vestibulum faucibus purus sit amet erat lobortis, tristique fringilla leo sodales. Interdum et malesuada fames ac ante.</p>
          <p>Aenean mattis tristique elementum. Duis massa tellus, tempus non fermentum at, venenatis et augue. Phasellus tristique purus sed sagittis interdum. Duis luctus sapien justo, vel viverra ex convallis et. Maecenas suscipit lacus ut lectus mattis ornare. Vestibulum faucibus purus sit amet erat lobortis, tristique fringilla leo sodales. Interdum et malesuada fames ac ante.</p>
        </div>
    </section>


@endsection




